<?php
/**
 * Featured Experts
 *
 * @package   Ajskelton\FeaturedExperts
 * @since     1.0.0
 * @author    ThoriumDesign
 * @link      thoriumdesign.com
 * @license   GNU General Public License 2.0+
 *
 * @wordpress-plugin
 * Plugin Name: Featured Experts
 * Plugin URI: https://anthonyskelton.com/featured-experts
 * Description: A WordPress Plugin to display a photo and information about a featured expert
 * Version: 1.0.4
 * Author: Thorium Design
 * Author URI: https://thoriumdesign.com
 * Text Domain: featured-experts
 * Requires WP: 4.7
 * Requires PHP: 5.5
 * Bitbucket Plugin URI: https://bitbucket.org/thoriumdesign/featured-experts
 * Bitbucket Branch: master
 *
 */
namespace Ajskelton\FeaturedExperts;

if ( ! defined( 'ABSPATH' ) ) {
	exit( "Oh, silly, there's nothing to see here." );
}

define( 'FEATURED_EXPERTS_PLUGIN', __FILE__ );
define( 'FEATURED_EXPERTS_DIR', plugin_dir_path( __FILE__ ) );
$plugin_url = plugin_dir_url( __FILE__ );
if( is_ssl() ) {
	$plugin_url = str_replace( 'http://', 'https://', $plugin_url );
}
define( 'FEATURED_EXPERTS_URL', $plugin_url );
define( 'FEATURED_EXPERTS_TEXT_DOMAIN', 'featured-experts' );

include( __DIR__ . '/src/plugin.php' );
