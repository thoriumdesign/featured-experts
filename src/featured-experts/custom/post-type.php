<?php
/**
 * CPT Handler
 *
 * @package   Ajskelton\FeaturedExperts\Module\Custom
 * @since     1.0.0
 * @author    ajskelton
 * @link      anthonyskelton.com
 * @license   GNU General Public License 2.0+
 */
namespace Ajskelton\FeaturedExperts\Module\Custom;

add_action( 'init', __NAMESPACE__ . '\register_featured_experts_custom_post_type' );
/**
 * Register the custom post type
 *
 * @since 1.0.0
 *
 * @return void
 */
function register_featured_experts_custom_post_type() {

	$features = get_all_post_type_features( 'post', array(
		'editor',
		'comments',
		'trackbacks',
		'custom-fields',
        'page-attributes',
        'post-formats',
        'post-attributes',
        'excerpt'
	) );

	$features[] = 'page-attributes';

	$args = array(
		'description' => 'Featured Experts',
		'label'       => __( 'Featured Experts', FEATURED_EXPERTS_MODULE_TEXT_DOMAIN ),
		'labels'      => get_featured_experts_post_type_labels_config( 'expert', 'Featured Expert', 'Featured Experts' ),
		'menu_icon'   => 'dashicons-groups',
		'public'      => true,
		'supports'    => $features,
		'has_archive' => true,
	);

	register_post_type( 'featured-experts', $args );
}

/**
 * Get the post type labels configuration
 *
 * @param string $post_type
 * @param string $singular_label
 * @param string $plural_label
 *
 * @return array
 */
function get_featured_experts_post_type_labels_config( $post_type, $singular_label, $plural_label ) {
	return array(
		'name'               => _x( $plural_label, 'post type general name', FEATURED_EXPERTS_MODULE_TEXT_DOMAIN ),
		'singular_name'      => _x( $singular_label, 'post type singular name', FEATURED_EXPERTS_MODULE_TEXT_DOMAIN ),
		'menu_name'          => _x( $plural_label, 'admin menu', FEATURED_EXPERTS_MODULE_TEXT_DOMAIN ),
		'name_admin_bar'     => _x( $singular_label, 'add new on admin bar', FEATURED_EXPERTS_MODULE_TEXT_DOMAIN ),
		'add_new'            => _x( 'Add New ', $post_type, FEATURED_EXPERTS_MODULE_TEXT_DOMAIN ),
		'add_new_item'       => __( 'Add New ' . $singular_label, FEATURED_EXPERTS_MODULE_TEXT_DOMAIN ),
		'new_item'           => __( 'New ' . $singular_label, FEATURED_EXPERTS_MODULE_TEXT_DOMAIN ),
		'edit_item'          => __( 'Edit ' . $singular_label, FEATURED_EXPERTS_MODULE_TEXT_DOMAIN ),
		'view_item'          => __( 'View ' . $singular_label, FEATURED_EXPERTS_MODULE_TEXT_DOMAIN ),
		'all_items'          => __( 'All ' . $plural_label, FEATURED_EXPERTS_MODULE_TEXT_DOMAIN ),
		'search_items'       => __( 'Search ' . $plural_label, FEATURED_EXPERTS_MODULE_TEXT_DOMAIN ),
		'parent_item_colon'  => __( 'Parent ' . $singular_label . ':', FEATURED_EXPERTS_MODULE_TEXT_DOMAIN ),
		'not_found'          => __( 'No ' . $plural_label . ' found.', FEATURED_EXPERTS_MODULE_TEXT_DOMAIN ),
		'not_found_in_trash' => __( 'No ' . $plural_label . ' found in Trash.', FEATURED_EXPERTS_MODULE_TEXT_DOMAIN )
	);
}


/**
 * Get all the post type features for the given post type.
 *
 * @since 1.0.0
 *
 * @param string $post_type Given post type
 * @param array $exclude_features Array of features to exclude
 *
 * @return array
 */
function get_all_post_type_features( $post_type = 'post', $exclude_features = array() ) {
	$configured_features = get_all_post_type_supports( $post_type );

	if ( ! $exclude_features ) {
		return array_keys( $configured_features );
	}

	$features = array();

	foreach ( $configured_features as $feature => $value ) {
		if ( in_array( $feature, $exclude_features ) ) {
			continue;
		}

		$features[] = $feature;
	}

	return $features;
}





