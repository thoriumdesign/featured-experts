<?php
/**
 * Customize the Photodex CPT Posts Page
 *
 * @package   Ajskelton\FeaturedExperts\Module\Admin
 * @since     1.0.0
 * @author    ajskelton
 * @link      anthonyskelton.com
 * @license   GNU General Public License 2.0+
 */

namespace Ajskelton\FeaturedExperts\Module\Admin;


add_action( 'load-post.php', __NAMESPACE__ . '\post_meta_boxes_setup' );
add_action( 'load-post-new.php', __NAMESPACE__ . '\post_meta_boxes_setup' );
/**
 * Setup the post meta boxes for Featured Experts Post Type. Add the meta boxes and
 * the saving processing
 *
 * @since 1.0.0
 *
 * @return void
 */
function post_meta_boxes_setup() {
	add_action( 'add_meta_boxes', __NAMESPACE__ . '\add_post_meta_boxes' );
	add_action( 'save_post', __NAMESPACE__ . '\save_featured_experts_meta', 10, 2 );
}

/**
 * Add post meta boxes on the Featured Experts Post Type
 *
 * @since 1.0.0
 *
 * @return void
 */
function add_post_meta_boxes() {
	add_meta_box(
		'featured_experts_info',
		esc_html__( 'Featured Expert Information', PHOTODEX_TEXT_DOMAIN ),
		__NAMESPACE__ . '\featured_experts_meta_box',
		'featured-experts',
		'normal',
		'high'
	);
}

/**
 * Write the HTML for the Featured Experts Meta Box
 *
 * @since 1.0.0
 *
 * @return void
 */
function featured_experts_meta_box() {

	wp_nonce_field( basename( __FILE__ ), 'featured_experts_meta_nonce' );
	$featured_expert_info = ( get_post_meta( get_the_ID(), 'featured_experts_info', true ) ) ? get_post_meta( get_the_ID(), 'featured_experts_info', true ) : array();
	?>
    <div class="inside">
        <p>
            <label for="featured_experts_company"><?php _e( 'Company / Description', FEATURED_EXPERTS_TEXT_DOMAIN ); ?></label>
            <input type="text" name="featured_experts_company" id="featured_experts_company" class="regular-text"
                   value="<?php echo $featured_expert_info['company'] ? $featured_expert_info['company'] : '' ?>">
        </p>
        <p>
            <label for="featured_experts_city"><?php _e( 'City', FEATURED_EXPERTS_TEXT_DOMAIN ); ?></label>
            <select name="featured_experts_city" id="featured_experts_city">
                <option value="LA" <?php if ( 'LA' == $featured_expert_info['city'] ) {
					echo 'selected="selected"';
				} ?>>LA
                </option>
                <option value="Sacramento" <?php if ( 'Sacramento' == $featured_expert_info['city'] ) {
					echo 'selected="selected"';
				} ?>>Sacramento
                </option>
                <option value="National" <?php if ( 'National' == $featured_expert_info['city'] ) {
		            echo 'selected="selected"';
	            } ?>>National
                </option>
            </select>
        </p>
        <p>
            <label for="featured_experts_link"><?php _e( 'Link', FEATURED_EXPERTS_TEXT_DOMAIN ); ?></label>
            <input type="url" name="featured_experts_link" id="featured_experts_link" class="regular-text"
                   value="<?php echo $featured_expert_info['link']; ?>">
        </p>
    </div>
	<?php
}

add_action( 'save_post', __NAMESPACE__ . '\save_featured_experts_meta', 10, 2 );
/**
 * Save/Update/Delete Post Meta for Featured Experts Post Type
 *
 * @since 1.0.0
 *
 * @param int $post_id
 * @param object $post
 *
 * @return mixed
 */
function save_featured_experts_meta( $post_id, $post ) {

	// Verify the nonce before processing
	if ( ! isset( $_POST['featured_experts_meta_nonce'] ) || ! wp_verify_nonce( $_POST['featured_experts_meta_nonce'], basename( __FILE__ ) ) ) {
		return $post_id;
	}


	// Get the post type object
	$post_type = get_post_type_object( $post->post_type );

	// Check if the current user has permission to edit the post.
	if ( ! current_user_can( $post_type->cap->edit_post, $post_id ) ) {
		return $post_id;
	}

	// Get the posted data and sanitize it
	$new_meta_value['company'] = ( isset( $_POST['featured_experts_company'] ) ? sanitize_text_field( $_POST['featured_experts_company'] ) : '' );
	$new_meta_value['city']    = ( isset( $_POST['featured_experts_city'] ) ? sanitize_text_field( $_POST['featured_experts_city'] ) : '' );
	$new_meta_value['link']    = ( isset( $_POST['featured_experts_link'] ) ? esc_url( $_POST['featured_experts_link'] ) : '' );

	// Get the meta key
	$meta_key = 'featured_experts_info';

	// Get the meta value of the custom field key
	$meta_value = get_post_meta( $post_id, $meta_key, true );

	// If a new meta value was added and there was no previous value, add it.
	if ( $new_meta_value && '' == $meta_value ) {
		add_post_meta( $post_id, $meta_key, $new_meta_value, true );
	} // If the new meta value does not match the old value, update it.
    elseif ( $new_meta_value && $new_meta_value != $meta_value ) {
		update_post_meta( $post_id, $meta_key, $new_meta_value );
	} // if there is no new meta value but an old value exists, delete it
    elseif ( '' == $new_meta_value && $meta_value ) {
		delete_post_meta( $post_id, $meta_key, $meta_value );
	}
}


add_filter( 'manage_edit-featured-experts_columns', __NAMESPACE__ . '\edit_featured_experts_columns' );
/**
 * Edit the admin column headers on the Featured Experts Post Class
 *
 * @since 1.0.0
 *
 * @return array
 */
function edit_featured_experts_columns() {

	$columns = array(
		'cb'        => '<input type="checkbox" />',
		'title'     => __( 'Title' ),
		'company'   => __( 'Company / Description' ),
		'city'      => __( 'City' ),
		'thumbnail' => __( 'Photo' )
	);

	return $columns;
}


add_action( 'manage_featured-experts_posts_custom_column', __NAMESPACE__ . '\manage_featured_experts_columns', 10, 2 );
/**
 * Manage the admin columns on the Featured Experts Post Class
 *
 * @since 1.0.0
 *
 * @param array $column
 * @param int $post_id
 *
 * @return void
 */
function manage_featured_experts_columns( $column, $post_id ) {

	$featured_experts_info = get_post_meta( $post_id, 'featured_experts_info', true );

	switch ( $column ) {
		case 'company' :

			echo $featured_experts_info['company'];

			break;

		case 'city' :
			echo $featured_experts_info['city'];

			break;

		case 'thumbnail':
			echo the_post_thumbnail( 'expert-thumbnail' );

			break;
	}
}


add_action( 'do_meta_boxes', __NAMESPACE__ . '\move_featured_meta_box' );
/**
 * Move the featured image meta box from the side to the normal at the top
 *
 * @since 1.0.0
 *
 * @return void
 */
function move_featured_meta_box() {
	remove_meta_box( 'postimagediv', 'featured-experts', 'side' );
	add_meta_box( 'postimagediv', __( 'Featured Image' ), 'post_thumbnail_meta_box', 'featured-experts', 'normal', 'high' );
}
