<?php
/**
 * Add a shortcode that returns a single photo based on post id or the entire photodex
 *
 * @package   Ajskelton\WpPhotodex\Module\Shortcode
 * @since     1.0.0
 * @author    ajskelton
 * @link      anthonyskelton.com
 * @license   GNU General Public License 2.0+
 */

namespace Ajskelton\FeaturedExperts\Module\Shortcode;

use Ajskelton\FeaturedExperts\Module\Util as Util;


add_shortcode( 'featured-experts', __NAMESPACE__ . '\process_the_shortcode' );
/**
 * Process the Shortcode to output the Featured Experts.
 *
 * @since 1.0.0
 *
 * @param array|string $user_defined_attributes User defined attributes for this shortcode instance
 * @param string|null $content Content between the opening and closing shortcode elements
 * @param string $shortcode_name Name of the shortcode
 *
 * @return string
 */
function process_the_shortcode( $user_defined_attributes, $content, $shortcode_name ) {
	$config = get_shortcode_configuration();

	$attributes = shortcode_atts(
		$config['defaults'],
		$user_defined_attributes,
		$shortcode_name
	);

	// Call the view file, capture it into the output buffer, and then return it.
	ob_start();

	render_experts( $attributes, $config );

	return ob_get_clean();
}


/**
 * Get the configuration for the shortcode
 *
 * @since 1.0.0
 *
 * @return array
 */
function get_shortcode_configuration() {
	return array(
		'views'    => array(
			'container' => FEATURED_EXPERTS_MODULE_DIR . '/views/container.php',
			'single'    => FEATURED_EXPERTS_MODULE_DIR . '/views/single.php'
		),
		'defaults' => array(
			'thumbnail_size' => 'thumbnail',
			'per_row'        => '3',
		)
	);
}

/**
 * Run the query and render the featured experts
 *
 * @since 1.0.0
 *
 * @param $attributes
 * @param $config
 */
function render_experts( $attributes, $config ) {

	$query = Util\query_featured_expert_posts();

	if ( ! $query->have_posts() ) {
		return;
	}

	include( $config['views']['container'] );

	wp_reset_postdata();
}

/**
 * Loop through the query and render out the experts
 *
 * @since 1.0.0
 *
 * @param \WP_Query $query
 * @param array $attributes
 * @param array $config
 *
 * @return void
 */
function loop_and_render_experts( \WP_Query $query, array $attributes, array $config ) {
	while ( $query->have_posts() ) {
		$query->the_post();

		$thumbnail_size = $attributes['thumbnail_size'];

		include( $config['views']['single'] );
	}
}