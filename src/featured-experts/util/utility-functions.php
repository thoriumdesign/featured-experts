<?php
/**
 * General Functions to use the plugin
 *
 * @package   Ajskelton\FeaturedExperts\Module\Util
 * @since     1.0.0
 * @author    ajskelton
 * @link      anthonyskelton.com
 * @license   GNU General Public License 2.0+
 */

namespace Ajskelton\FeaturedExperts\Module\Util;

/**
 * Return a WP Query of all of the Featured Expert posts, sorted by their
 * pokedex number
 *
 * @since 1.0.0
 *
 * @return \WP_Query
 */
function query_featured_expert_posts() {
	$config_args = array(
		'post_type'      => 'featured-experts',
		'posts_per_page' => - 1,
		'orderby'        => 'menu_order',
		'order'          => 'ASC'
	);

	return new \WP_Query( $config_args );
}