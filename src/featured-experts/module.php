<?php
/**
 * Featured Experts Module Handler
 *
 * @package   Ajskelton\FeaturedExperts\Module
 * @since     1.0.0
 * @author    ajskelton
 * @link      anthonyskelton.com
 * @license   GNU General Public License 2.0+
 */
namespace Ajskelton\FeaturedExperts\Module;

define( 'FEATURED_EXPERTS_MODULE_TEXT_DOMAIN', FEATURED_EXPERTS_TEXT_DOMAIN );
define( 'FEATURED_EXPERTS_MODULE_DIR', __DIR__ );
define( 'FEATURED_EXPERTS_VERSION', '1.0.0' );

add_action( 'wp_enqueue_scripts', __NAMESPACE__ . '\add_my_stylesheet' );
/**
 * Enqueue Styles for front end of the photodex
 *
 * @since 1.0.0
 */
function add_my_stylesheet() {
	wp_enqueue_style( 'featured-experts', FEATURED_EXPERTS_URL . '/src/featured-experts/assets/dist/css/style.css', false, FEATURED_EXPERTS_VERSION, false  );
}

add_action( 'init', __NAMESPACE__ . '\add_featured_expert_thumbnail_size' );
function add_featured_expert_thumbnail_size() {
	add_image_size( 'expert-thumbnail', '300', '300', true );
}

/**
 * Autoload plugin files
 *
 * @since 1.0.0
 *
 * @return void
 */
function autoload() {
	$files = array(
		'admin/featured-experts-page.php',
		'custom/post-type.php',
		'shortcode/shortcode.php',
		'util/utility-functions.php',
	);

	foreach ( $files as $file ) {
		include( __DIR__ . '/' . $file );
	}
}

autoload();

register_activation_hook( FEATURED_EXPERTS_PLUGIN, __NAMESPACE__ . '\activate_the_plugin' );
/**
 * Initialize the rewrites for our new custom post type
 * upon activation
 *
 * @since 1.0.0
 *
 * @return void
 */
function activate_the_plugin() {
	Custom\register_featured_experts_custom_post_type();

	flush_rewrite_rules();
}

register_deactivation_hook( FEATURED_EXPERTS_PLUGIN, __NAMESPACE__ . '\deactivate_plugin' );
/**
 * The plugin is deactivating. Delete out the rewrite rules option.
 *
 * @since 1.0.0
 *
 * @return void
 */
function deactivate_plugin() {
	delete_option( 'rewrite_rules' );
}

register_uninstall_hook( FEATURED_EXPERTS_PLUGIN, __NAMESPACE__ . '\uninstall_plugin' );
/**
 * Plugin is being uninstalled. Clean up after ourselves
 *
 * @since 1.0.0
 *
 * @return void
 */
function uninstall_plugin() {
	delete_option( 'rewrite_rules' );
}
