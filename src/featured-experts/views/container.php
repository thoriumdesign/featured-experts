<?php use Ajskelton\FeaturedExperts\Module\Shortcode as Shortcode; ?>

<div class="experts--container featured-experts">
    <?php Shortcode\loop_and_render_experts( $query, $attributes, $config ); ?>
</div>
