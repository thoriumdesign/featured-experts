<?php
$featured_experts_info = get_post_meta( get_the_ID(), 'featured_experts_info', true );
?>
<div class="featured-expert experts-per-row-<?php echo $attributes['per_row']; ?>">
    <a href="<?php echo esc_url($featured_experts_info['link']); ?>">
	    <?php echo get_the_post_thumbnail( get_the_ID(), 'expert-thumbnail' ); ?>
        <div class="featured-expert__text-wrap">
            <p class="title"><?php echo get_the_title(); ?></p>
            <p class="company"><?php echo $featured_experts_info['company'] ?></p>
            <p class="city"><span class="pill"><?php echo $featured_experts_info['city']; ?></span></p>
        </div>
    </a>
</div>